package com.casumo.controller;

import com.casumo.domain.User;
import com.casumo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@RestController
@RequestMapping(path = "/users")
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping(value={"/all"})
    public Page<User> getAllUsers(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @GetMapping(value = "/search/byname/{searchTerm}")
    public Iterable<User> searchByFullName(
            @PathVariable String searchTerm,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "offset", required = false) Integer offset) {
        if ( limit == null ) limit = 10;
        if ( offset == null ) offset = 0;

//      TODO: Figure out a way to add total count to response.
        return userRepository.searchByFullName("%"+searchTerm+"%", limit, offset);
    }

    @GetMapping(value = "/filter")
    public Iterable<User> filterAll(
            @RequestParam(value = "firstName", required = false) String firstName,
            @RequestParam(value = "lastName", required = false) String lastName,
            @RequestParam(value = "segment", required = false) String segment,
            @RequestParam(value = "depomin", required = false) Integer depomin,
            @RequestParam(value = "depomax", required = false) Integer depomax,
            @RequestParam(value = "withmin", required = false) Integer withmin,
            @RequestParam(value = "withmax", required = false) Integer withmax,
            @RequestParam(value = "balmin", required = false) Integer balmin,
            @RequestParam(value = "balmax", required = false) Integer balmax,
            @RequestParam(value = "datemin", required = false) Long datemin,
            @RequestParam(value = "datemax", required = false) Long datemax,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "offset", required = false) Integer offset) {
        if ( firstName == null ) {
            firstName = "%";
        } else {
            firstName = "%"+firstName+"%";
        }
        if ( lastName == null ) {
            lastName = "%";
        } else {
            lastName = "%"+lastName+"%";
        }
        if ( segment == null ) segment = "%";
        if ( depomin == null ) depomin = 0;
        if ( depomax == null ) depomax = Integer.MAX_VALUE;
        if ( withmin == null ) withmin = 0;
        if ( withmax == null ) withmax = Integer.MAX_VALUE;
        if ( balmin == null ) balmin = 0;
        if ( balmax == null ) balmax = Integer.MAX_VALUE;
        if ( datemin == null ) datemin = 0l;
        if ( datemax == null ) datemax = Long.MAX_VALUE;
        if ( limit == null ) limit = 20;
        if ( offset == null ) offset = 0;

        return userRepository.filterBy(firstName, lastName, segment, depomin, depomax, withmin, withmax, balmin, balmax, datemin, datemax, limit, offset);
    }


    @GetMapping(value = "/search/bysegment/{searchTerm}")
    public Page<User> searchBySegment(@PathVariable Integer searchTerm, Pageable pageable) {
        return userRepository.findBySegment(searchTerm, pageable);
    }

    @GetMapping(value = "/search/bytotaldeposits")
    public Page<User> searchByTotalDeposits(
            Pageable pageable,
            @RequestParam(value = "min") Double min,
            @RequestParam(value = "max") Double max) {
        return userRepository.findByTotalDepositsBetween(min, max, pageable);
    }

    @GetMapping(value = "/search/bytotalbets")
    public Page<User> searchByTotalBets(
            Pageable pageable,
            @RequestParam(value = "min") Double min,
            @RequestParam(value = "max") Double max) {
        return userRepository.findByTotalBetsBetween(min, max, pageable);
    }

    @GetMapping(value = "/search/bytotalwithdraws")
    public Page<User> searchByTotalWithdraws(
            Pageable pageable,
            @RequestParam(value = "min") Double min,
            @RequestParam(value = "max") Double max) {
        return userRepository.findByTotalWithdrawsBetween(min, max, pageable);
    }

    @GetMapping(value = "/search/bycurrentbalance")
    public Page<User> searchByCurrentBalance(
            Pageable pageable,
            @RequestParam(value = "min") Double min,
            @RequestParam(value = "max") Double max) {
        return userRepository.findByCurrentBalanceBetween(min, max, pageable);
    }

    @GetMapping(value = "/search/byregistered")
    public Page<User> searchByRegistered(
            Pageable pageable,
            @RequestParam(value = "min") Long min,
            @RequestParam(value = "max") Long max
            ) {
        Date minDate = new Date(min);
        Date maxDate = new Date(max);
        return userRepository.findByRegisteredBetween(minDate, maxDate, pageable);
    }

//    @RequestMapping(
//            value = "/**",
//            method = RequestMethod.OPTIONS
//    )
//    public ResponseEntity handle() {
//        return new ResponseEntity(HttpStatus.OK);
//    }

//    @GetMapping(value = "/search")
//    public Iterable<User> searchByFullName(
//            @RequestParam("fullname") String fullName,
//            @RequestParam("segment") String segment) {
//        String queryString = "SELECT * FROM user WHERE ";
//        Boolean first = true;
//
//        if ( fullName != null) {
//            queryString += "CONCAT(first_name, ' ', last_name) LIKE '"+ fullName +"' ";
//            first = false;
//        }
//        if ( segment != null ) {
//            if ( !first ) queryString += "AND ";
//
//            queryString += "segment = '"+segment+"' ";
//        }
//        return userRepository.searchByAll(queryString);
//    }

}
