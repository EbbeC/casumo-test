package com.casumo.repository;

import com.casumo.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    Page<User> findBySegment(Integer id, Pageable pageable);

    User findById(Integer id);

    //  NOTE: Can't do pagination with nativeQueries.
    @Query(value = "SELECT * FROM users WHERE CONCAT(first_name, ' ', last_name) LIKE ? LIMIT ? OFFSET ?", nativeQuery = true)
    Iterable<User> searchByFullName(String searchTerm, Integer limit, Integer offset);

    //  NOTE: Can't do pagination with nativeQueries.
    @Query(value = "SELECT * FROM users WHERE first_name LIKE ? AND last_name LIKE ? AND segment LIKE ? AND total_deposits >= ? AND total_deposits <= ? AND total_withdraws >= ? AND total_withdraws <= ? AND current_balance >= ? AND current_balance <= ? AND registered >= ? AND registered <= ? LIMIT ? OFFSET ?", nativeQuery = true)
    Iterable<User> filterBy(String firstName, String lastName, String segment, Integer depomin, Integer depomax, Integer withmin, Integer withmax, Integer balmin, Integer balmax, Long datemin, Long datemax, Integer limit, Integer offset);

//    //  NOTE: Can't do pagination with nativeQueries.
//    @Query(value = "SELECT * FROM users WHERE first_name LIKE ? AND last_name LIKE ? AND segment LIKE ? AND total_deposits >= ? AND total_deposits <= ? AND total_withdraws >= ? AND total_withdraws <= ? AND current_balance >= ? AND current_balance <= ? LIMIT ? OFFSET ?", nativeQuery = true)
//    Iterable<User> filterBy(String firstName, String lastName, String segment, Integer depomin, Integer depomax, Integer withmin, Integer withmax, Integer balmin, Integer balmax, Integer limit, Integer offset);

    // NOTE: To sort pages add &sort=columnName,desc to the url
    Page<User> findByTotalDepositsBetween(Double min, Double max, Pageable pageable);

    Page<User> findByTotalBetsBetween(Double min, Double max, Pageable pageable);

    Page<User> findByTotalWithdrawsBetween(Double min, Double max, Pageable pageable);

    Page<User> findByCurrentBalanceBetween(Double min, Double max, Pageable pageable);

    Page<User> findByRegisteredBetween(Date min, Date max, Pageable pageable);
}

