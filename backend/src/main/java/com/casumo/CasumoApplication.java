package com.casumo;

import com.casumo.domain.User;
import com.casumo.repository.UserRepository;
import com.github.javafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Date;

@SpringBootApplication
public class CasumoApplication {

	private Faker faker = new Faker();

	public static void main(String[] args) {
		SpringApplication.run(CasumoApplication.class, args);
	}

	@Bean
	public CommandLineRunner initDb(UserRepository userRepository) {
		return (args) -> {
			userRepository.deleteAll();
			for ( int i=0; i<1000; i++ ) {
				User u = new User(faker.name().firstName(), faker.name().lastName());
				u.setPassword(faker.crypto().sha1());
				u.setSegment(faker.number().numberBetween(1,6));
				double deposit = faker.number().randomDouble(2,0,6000);
				u.setTotalDeposits(deposit);
				u.setTotalBets(faker.number().randomDouble(2, (int) deposit, 30000));
				u.setTotalWithdraws(faker.number().randomDouble(2, 0, 10000));
				u.setCurrentBalance(faker.number().randomDouble(2, 0, 10000));
				u.setRegistered(faker.date().between(new Date(946684800000l), new Date(1499644799000l)));
				userRepository.save(u);
			}
		};
	}

//	@Configuration
//	@EnableWebMvc
//	public class WebConfig extends WebMvcConfigurerAdapter {
//
//		@Override
//		public void addCorsMappings(CorsRegistry registry) {
//			registry.addMapping("/**");
//		}
//	}
}
