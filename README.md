# README #

Simple single page app using knockoutJS at the front end and a Spring Boot powered Rest API at the backend. Have fun!

## First of all ##

* Clone repository with: git clone git@bitbucket.org:EbbeC/casumo-test.git

### Setting up the backend. ###

* Put MySQL up and running (I like [this](https://gist.github.com/nrollr/3f57fc15ded7dddddcc4e82fe137b58e) way)
* Open project from casumo-test/backend directory with IntelliJ or STS.
* Bring in the Maven dependencies.
* Go to src->main->resources and open application.properties
* Update your information for MySQL
* Run the project.

### Setting up the frontend. ###

* Make sure you have [node/npm](https://nodejs.org/en/download/) installed.
* cd into casumo-test/frontend directory.
* type: npm install
* to start dev server: npm start
* open http://localhost:8000 on your browser.
* Notice! Must have Java project running on localhost:8080 for this to work