/*
****************************************************************************
* Important notes
****************************************************************************
* /
- Listeners can only modify state and then call urlBuilder() which will
  then build the appropriate url based on the state.

/*----------------------------------------
TODO:
- Setup pagination by doing a count-api
- HTML/CSS for navigation filters
- Rest of the filters implemented in KO
----------------------------------------*/
/*
****************************************************************************
* Pulling in the dependencies
****************************************************************************
*/
var $ 		= require('jquery');
var ko 		= require('knockout');
var Sammy 	= require('sammy');
var moment	= require('moment');
var helpers	= require('./helpers');

/*
****************************************************************************
* Knockout
****************************************************************************
*/
ko.bindingHandlers.dateText = {
	init: null,
	update: function(element, valueAccessor) {
		var currentValue = valueAccessor();
		var final = moment(currentValue).format('YYYY-MM-DD HH:mm:ss');
		$(element).text(final);
	}
};
ko.bindingHandlers.currencyText = {
	init: null,
	update: function(element, valueAccessor) {
		var currentValue = valueAccessor();
		var final = currentValue.toFixed(2).toString().replace('.', ',') + " €";
		$(element).text(final);
	}
}
ko.bindingHandlers.segmentText = {
	init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
		this.list = bindingContext.$root.segmentList();
	},
	update: function(element, valueAccessor) {
		var currentValue = parseInt(valueAccessor());
		var final = this.list[currentValue - 1].name;
		$(element).text(final);
	}
}

function NewUsers() {
	// Data
	var self = this;
	self.userlist 		= ko.observableArray();
	self.currentPage 	= ko.observable();
	// Dealing with tables
	self.tablePageNum 	= ko.observable();
	self.tableFirstPage = ko.observable();
	self.tableLastPage 	= ko.observable();
	self.tableSorting	= ko.observable(null);
	self.sortDirection	= ko.observable('asc');
	/*
	****************************************************************************
	* Filters
	****************************************************************************
	*/
	var filter = function(filterObj) {
		this.type = filterObj.type;
		this.value = filterObj.value;
		this.min = filterObj.min;
		this.max = filterObj.max;
	}
	self.filterList = ko.observableArray();
	/*
	****************************************************************************
	* Setup segments
	****************************************************************************
	*/
	var segment = function(id, name) {
		this.id = id;
		this.name = name;
	}
	// Used to display and handle segments
	self.segmentList	= ko.observableArray([
		new segment(1,'Non-depositor'),
		new segment(2,'Normal'),
		new segment(3,'VIP'),
		new segment(4,'Very VIP'),
		new segment(5,'Churned')
	]);
	self.selectedSegment = ko.observable();
	/*
	****************************************************************************
	* Segment update
	* TODO: Instead of doing segments alone, let's do a generic function
	* to add and remove filters dynamically.
	****************************************************************************
	* /
	
	self.selectedSegment.subscribe(function(newValue) {

		self.sortDirection('desc');
		self.tableSorting('id');

		if ( newValue )  {
			// TEMPORARY HACK - Start
			self.filterList.removeAll();
			// TEMPORARY HACK - End
			self.filterList.push(new filter({ value: newValue.id, type: 'segment' }));
		} else {
			// Normally we would just delete the segment from filterlist
			// but now I'm just going to empty the lot.
			self.filterList.removeAll();
		}
		self.urlBuilder();
	});

	/*
	****************************************************************************
	* Behaviors
	****************************************************************************
	*/
	self.goToPage = function(page, event) { location.hash = page; };

	self.tableGoToPage = function(direction, root, event) { 

		if ( (direction == 'next' && !self.tableLastPage()) || 
			 (direction == 'prev' && !self.tableFirstPage()) ) {

			if ( direction == 'next' ) self.tablePageNum(parseInt(self.tablePageNum()) + 1);
			else self.tablePageNum(parseInt(self.tablePageNum()) - 1);

			self.urlBuilder();
		}
	}
	/*
	 * All url's will follow a predefined structure which is:
	 * /<pageName>/<tablePageNumber>/filterby/<prefix=postfix_value,postfix_value|prefix=_value>/sortby/<column>/<asc|desc>
	 * Listeners MUST modify the states to automatically make that happen in the urlBuilder();
	 * This is why urlBuilder() accepts no parameters.
	 */
	self.urlBuilder = function() {
		// Start with current page
		var urlString = "/"+self.currentPage()+'/'+self.tablePageNum();
		// If any filter is set add them
		if ( self.filterList().length ) {
			urlString += '/filterby/';
			// Let's build like /type=name_val,name_val|type=name_val/
			var filterString = '';
			$.each(self.filterList(), function(index, value) {
				if ( value.value ) {
					filterString += value.type+'=_'+value.value+'|';
				} else {
					if ( value.min ) {
						filterString += value.type+'=min_'+value.min;
						if ( !value.max ) filterString += '|';
						else filterString += ',';
					}
					if ( value.max ) {
						if ( !value.min ) filterString +=  value.type+'=';
						filterString += 'max_'+value.max+'|';
					}
				}
			});
			filterString = filterString.substring(0, filterString.length - 1);
			urlString += filterString;
		}
		// If sorting is set add that (currently must be set or nothing works)
		if ( !self.tableSorting() ) self.tableSorting('id');
		// if ( self.tableSorting() ) {

			urlString += '/sortby/'+self.tableSorting();

			if ( self.tableSorting() == 'id' || 
				 self.tableSorting() == 'firstName' || 
				 self.tableSorting() == 'lastName' ) {

				var tempDirection;
				if ( self.sortDirection() == 'asc' ) tempDirection = '/desc';
				else tempDirection = '/asc';
				
				urlString += tempDirection;
			} else {
				urlString += '/'+self.sortDirection();
			}
		// }
		location.hash = urlString;
	}
	self.sortBy = function(column) {
		// If you chose the same column again, let's switch the direction.
		if ( column == self.tableSorting() ) {
			if ( self.sortDirection() == 'asc' ) self.sortDirection('desc');
			else self.sortDirection('asc');
		} else {
			self.sortDirection('desc');
		}
		self.tableSorting(column);

		self.urlBuilder();
	}
	self.resetFilters = function() {
		var theForm = $('#filterform');
		theForm.find("input[type=text], textarea").val("");
		theForm.find("input[type=radio]").prop('checked', false);
		self.filterList.removeAll();
		self.sortDirection('desc');
		self.urlBuilder();
	}
	self.removeFilter = function(filter) {
		console.log('totally just removed that filter.');
	}
	self.addRemoveFilters = function(filters) {
		self.filterList.removeAll();

		var resultCollection = {};

		// Let's sort the filters out a bit so they are easier to handle.
		$.each(filters, function(index, value) {
			// self.filterList.push(new filter({ value: newValue.id, type: 'segment', min: 12, max, 1234 }));
			console.log(value);
			if ( value && !( index == 'segment' && value == '0') ) {
				var pieces = index.split('-');
				if ( pieces.length > 1 ) {
					if ( resultCollection[pieces[0]] ) {
						resultCollection[pieces[0]][pieces[1]] = value;
					} else {
						resultCollection[pieces[0]] = {};
						resultCollection[pieces[0]][pieces[1]] = value;
					}
				} else {
					resultCollection[pieces[0]] = value;
				}
			}
		});
		// And let's actually handle the filters and add them to the list.
		$.each(resultCollection, function(index, value) {
			var newFilter = {};
			newFilter['type'] = index;
			if ( typeof value == 'string' ) {
				newFilter['value'] = value;
				newFilter['min'] = null;
				newFilter['max'] = null;
			} else {
				$.each(value, function(innerIndex, innerValue) {
					newFilter[innerIndex] = innerValue;
				});
				newFilter['value'] = null;
			}
			self.filterList.push(new filter(newFilter));
		});
		self.urlBuilder();
	}
	self.handleFilterForm = function(form) {
		var returnArray = {};
		for (var i = 0; i < form.length; i++){
			var el = form[i];
			if ( el.type == 'radio' ) {
				if ( el.checked == true ) {
					returnArray[el['name']] = el['value'];
				}
			} else if ( el.type == 'submit' ) {
				// Do nothing
			} else {
				returnArray[el['name']] = el['value'];
			}
		}
		self.addRemoveFilters(returnArray);
	}
	
	/*
	****************************************************************************
	* Routes
	****************************************************************************
	*/
	Sammy(function() {
		// Users root
		this.get("#/users", function() {
			self.currentPage('users');
			self.tablePageNum(0);
			self.tableFirstPage(true);
			self.tableLastPage(false);
			self.tableSorting('id');
			self.sortDirection('asc');

			helpers.jax("users/all").then(
				function(response) { 
					helpers.handleResponse(response, self); 
				},
				function(error) { console.log(error); }
			);
		});

		/*
		****************************************************************************
		* User page
		****************************************************************************
		*/
		this.get("#/users/:num", function() {
			self.currentPage('users');
			self.tablePageNum(this.params.num);

			helpers.jax("users/all?page=" + this.params.num).then(
				function(response) { 
					helpers.handleResponse(response, self); 
				},
				function(error) { console.log(error); }
			);
		});
		/*
		****************************************************************************
		* Users page with sorting
		****************************************************************************
		*/
		this.get("#/users/:page/sortby/:column/:direction", function() {
			self.currentPage('users');
			self.tablePageNum(this.params.page);

			helpers.jax("users/all?page="+this.params.page+"&sort="+this.params.column+","+this.params.direction).then(
				function(response) { 
					helpers.handleResponse(response, self); 
				},
				function(error) { console.log(error); }
			);
		});
		/*
		****************************************************************************
		* Users page with filters and sorting
		****************************************************************************
		*/
		this.get("#/users/:page/filterby/:filters/sortby/:column/:direction", function() {
			self.currentPage('users');
			self.tablePageNum(this.params.page);

			var finalUrl = 'users/filter?';
			// Let's start mining for the parameters..
			var searchFilters = this.params.filters.split('|');

			// First parse the filters (currently only segment)
			$.each(searchFilters, function(index, value) {
				var singleFilter = value.split('=');
				// Filter category
				var filterCategory = singleFilter[0];
				// Filter names and values
				var namesAndValues = singleFilter[1].split(',');
				$.each(namesAndValues, function(index, value) {
					finalUrl += filterCategory;
					var nameAndValue = value.split('_');
					finalUrl += nameAndValue[0] + "=" + nameAndValue[1] + "&";
				});
			});
			// Let's remove the trailing &
			finalUrl = finalUrl.substring(0, finalUrl.length-1);
			// Final touches with the sorting column and direction...
			// finalUrl += "&sort="+this.params.column+","+this.params.direction;

			// ...and voila! Url done. Let's use it!
			helpers.jax(finalUrl).then(
				function(response) { 
					helpers.handleNonPagedResponse(response, self); 
				},
				function(error) { console.log(error); }
			);			
		});

		// Reports root
		this.get("#/reports", function() {
			self.currentPage('reports');
			self.tablePageNum(0);
			self.sortDirection(true);
		});

		// Homepage
		this.get('#/home', function() { 
			self.currentPage('home');

			helpers.jax("users/all?sort=registered,desc&size=10").then(
				function(response) { helpers.handleResponse(response, self); },
				function(error) { console.log(error); }
			);
		});

		this.get('', function() { this.app.runRoute('get', '#/home') });

		// Make Sammy.js leave the forms alone!
		this.post("#/formsubmit", function() { return false; });
		this._checkFormSubmission = function(form) { return false; };
	}).run('');
	
};

ko.applyBindings(new NewUsers());