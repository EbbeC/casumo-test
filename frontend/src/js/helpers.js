var $ = require('jquery');

var API_URL = "http://localhost:8080/";

var helpers = {
	jax: function(endpoint, type, data) {

		return new Promise(function(resolve, reject) {
			endpoint = endpoint ? endpoint : 'users/all';
			type = type ? type : 'get';

			$.ajax({
				url: `${API_URL}${endpoint}`,
				type: type,
				contentType: 'json',
				data: data,
				error : function(XMLHttpRequest, textStatus, errorThrown){
					reject(textStatus);
				},
				success : function(response) {
					resolve(response);
				}
			});
		});
	},
	handleResponse: function(response, self, callback) {
		callback = callback ? callback : function(){};
		
		self.userlist.removeAll();
		$.each(response.content, function(index, value) {
			self.userlist.push(value);
		});

		self.tablePageNum(response.number);
		self.tableFirstPage(response.first);
		self.tableLastPage(response.last);
		callback();
	},
	handleNonPagedResponse: function(response, self, callback) {
		callback = callback ? callback : function(){};
		
		self.userlist.removeAll();
		$.each(response, function(index, value) {
			self.userlist.push(value);
		});	

		/*
		****************************************************************************
		* TODO: We will need to create our own paging system here.
		* Idea: Let's make an api which accepts the same parameters but only 
		* returns a total count of values. Use that in this function.
		****************************************************************************
		*/
		// self.tablePageNum(0);
		// self.tableFirstPage(true);
		// self.tableLastPage(false);
		callback();
	}
};

module.exports = helpers;